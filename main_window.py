from PyQt5 import QtWidgets, QtCore
import matplotlib
import itertools
import os

from gui.control_panel_widget import ControlPanelWidget
from gui.mpl_widget import MplWidget
from gui.panel_widget import PanelWidget
from gui.traces_list_widget import TracesListWidget
from gui.log_widget import LogWidget
from processing.mediator import Mediator


colors = {"r" : [1, 0, 0], "g" : [0, 1, 0], "b" : [0, 0, 1], "y" : [1, 1, 0],
          "o" : [1,  0.5, 0], "a" : [0, 1, 1], "p" : [1, 0, 1]}


class MainWindow(QtWidgets.QMainWindow):

    # _call_load_data_file_signal = QtCore.pyqtSignal(name="callLoadDataFile")

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)

        self.mediator = None

        self.panel_widget         = PanelWidget(self)
        self.control_panel_widget = ControlPanelWidget(self)
        self.mpl_widget           = MplWidget(self)
        self.traces_list_widget   = TracesListWidget(self)
        self.log_widget           = LogWidget(self)

        self._left_panel_layer = QtWidgets.QVBoxLayout()
        self._left_panel_layer.addWidget(self.traces_list_widget, 4)
        self._left_panel_layer.addWidget(self.log_widget, 2)

        self._main_layer = QtWidgets.QGridLayout()
        self._main_layer.addWidget(self.panel_widget,         0, 0, 1, 2)
        self._main_layer.addLayout(self._left_panel_layer,    1, 0, 1, 1)
        self._main_layer.addWidget(self.mpl_widget,           1, 1, 1, 4)
        self._main_layer.addWidget(self.control_panel_widget, 1, 5, 1, 1)

        self._main_widget = QtWidgets.QWidget(self)
        self._main_widget.setLayout(self._main_layer)

        self.setCentralWidget(self._main_widget)

        self.data_file = None

        self.display_tvd_state  = False
        self.display_fit_state = False
        self.display_thr_state = False
        self.display_interv_state = False

        self.panel_widget.load_data_button.clicked.connect(self.load_file_dialog)
        self.panel_widget.state_button.clicked.connect(self.load_state)
        self.control_panel_widget.find_jumps_button.clicked.connect(self.run_make_jumps)
        self.control_panel_widget.advanced_fit_button.clicked.connect(self.compute_fit)
        self.control_panel_widget.open_prob_update_button.clicked.connect(self.update_open_prob_threshold)
        self.control_panel_widget.advanced_fit_all_button.clicked.connect(self.compute_fit_all)
        self.control_panel_widget.style_update_button.clicked.connect(self.update_style)
        self.traces_list_widget.button_group.buttonClicked.connect(self.update_mpl_widget)
        self.mpl_widget.mplOnMouseClick.connect(self.correct_jump)
        self.traces_list_widget.onTvdChboxes.connect(self.change_display_tvd_state)
        self.traces_list_widget.onBaseChboxes.connect(self.change_display_base_state)
        self.traces_list_widget.onThrChboxes.connect(self.change_display_thr_state)
        self.traces_list_widget.onIntervChboxes.connect(self.change_display_interv_state)
        self.traces_list_widget.onIncludeChboxes.connect(self.update_mpl_widget)
        self.panel_widget.save_backup_button.clicked.connect(self.backup)
        self.panel_widget.save_result_button.clicked.connect(self.save_results)
        #self.control_panel_widget.updateThr.connect(self.update_open_prob_threshold)

        self._data_color = [0, 0, 1]
        self._sub_color  = [1, 0.5, 0]
        self._fit_color  = [1, 0, 1]
        self._base_color = [1, 1, 0]
        self._thr_color  = [1, 0, 0]

        self._zoom_state = []

    def load_file_dialog(self):
        data_file = str(QtWidgets.QFileDialog.getOpenFileName(self, "Open data", './', "*")[0])

        if data_file == '':
            return
        self.data_file = data_file

        self.mediator.data_file = data_file

        self.panel_widget.load_data_label.setText(data_file)

        self.mediator.load_data_mediator()

        # self.plot_traces(self.mediator.data)
        self.fill_list(self.mediator.data.shape[1])

        self.update_mpl_widget()

    def load_state(self):
        backup_dir = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Open backup", './'))

        if backup_dir == '':
            return

        tmax = float(str(self.control_panel_widget.find_jumps_tmax_edit.text()))
        self.mediator.load_state(backup_dir, tmax)

        self.update_mpl_widget()

    def run_make_jumps(self):
        t_med = float(str(self.control_panel_widget.find_jumps_t_med_edit.text()))
        thr   = float(str(self.control_panel_widget.find_jumps_thr_edit.text()))
        sec   = float(str(self.control_panel_widget.find_jumps_sec_edit.text()))
        tmax = float(str(self.control_panel_widget.find_jumps_tmax_edit.text()))

        self.mediator.res_folder = self.initialize_res_folder()
        self.mediator.data_base_name = self.panel_widget.data_base_edit.text()

        self.mediator.run_make_jumps(t_med, thr, sec, tmax)

        self.update_mpl_widget()

    def initialize_res_folder(self):
        res_folder = self.panel_widget.save_results_edit.text()
        if not os.path.exists(res_folder) and res_folder:
            os.makedirs(res_folder)
        return res_folder

    def correct_jump(self):
        state, trace = self.control_panel_widget.get_current_state_and_trace()
        trace = self.traces_list_widget.button_group.checkedId()
        if state == "add_p":
            self.mediator.add_positive(trace, self.mpl_widget.get_mouse_coordinates())
        elif state == "add_n":
            self.mediator.add_negative(trace, self.mpl_widget.get_mouse_coordinates())
        elif state == "del_p":
            self.mediator.del_positive(trace, self.mpl_widget.get_mouse_coordinates())
        elif state == "del_n":
            self.mediator.del_negative(trace, self.mpl_widget.get_mouse_coordinates())
        else:
            return

        self.save_zoom_state()
        self.update_mpl_widget()
        self.load_zoom_state()

    def compute_fit(self):
        state = self.control_panel_widget.get_current_fit_state()
        trace = self.traces_list_widget.button_group.checkedId()
        if state == "fitexp":
            self.mediator.fitexp(trace)
        elif state == "fitexp2":
            self.mediator.fitexp2(trace)
        elif state == "fitln":
            self.mediator.fitln(trace)
        elif state == "fitlin":
            self.mediator.fitlin(trace)
        self.update_mpl_widget()

    def compute_fit_all(self):
        state = self.control_panel_widget.get_current_fit_state()
        trace = self.traces_list_widget.button_group.checkedId()
        if state == "fitexp":
            self.mediator.fitexp_all(trace)
        elif state == "fitexp2":
            self.mediator.fitexp2_all(trace)
        elif state == "fitln":
            self.mediator.fitln_all(trace)
        elif state == "fitlin":
            self.mediator.fitlin_all(trace)
        self.update_mpl_widget()

    def update_mpl_widget(self, thr_upd=False):
        i = self.traces_list_widget.button_group.checkedId()
        self.mpl_widget.clear_plot()
        fine_p = self.mediator.fine_p
        fine_n = self.mediator.fine_n

        if not self.mediator.time.size:
            return

        if thr_upd is True:
            self.update_open_prob_threshold()

        self.control_panel_widget.restore_tmed_value(i)

        if not self.traces_list_widget.check_trace_state(i):
            self.mpl_widget.plot_data(self.mediator.time, self.mediator.data[:, i], [0.5, 0.5, 0.5], float(self.control_panel_widget.transp_data_edit.text()))
            return

        if self.mediator.sigma.size > 0:
            self.mpl_widget.plot_data(self.mediator.time, self.mediator.sub[:, i], self._sub_color, float(self.control_panel_widget.transp_sub_edit.text()))
            self.mpl_widget.plot_data(self.mediator.time, self.mediator.data[:, i], self._data_color, float(self.control_panel_widget.transp_data_edit.text()))
            self.mpl_widget.plot_data(self.mediator.time, self.mediator.base[:, i], self._base_color, float(self.control_panel_widget.transp_base_edit.text()))
        else:
            self.mpl_widget.plot_data(self.mediator.time, self.mediator.data[:, i], self._data_color, float(self.control_panel_widget.transp_data_edit.text()))
        if i in fine_p:
            self.mpl_widget.plot_jumps(fine_p[i], self.mediator.bumps_p[i], self.mediator.time, self.mediator.data[:, i], "p", self._data_color)
        if i in fine_n:
            self.mpl_widget.plot_jumps(fine_n[i], self.mediator.bumps_n[i], self.mediator.time, self.mediator.data[:, i], "n", self._data_color)
        if self.display_fit_state:
            self.mpl_widget.plot_data(self.mediator.time, self.mediator.fit[:, i], self._fit_color, float(self.control_panel_widget.transp_fit_edit.text()))
        thr     = self.mediator.get_trace_open_prob_thr(i)
        oprob = self.mediator.get_trace_open_prob_val(i)
        if self.display_thr_state:
            self.mpl_widget.plot_data(self.mediator.time, [thr]*len(self.mediator.time), color=self._thr_color, alpha=float(self.control_panel_widget.transp_thr_edit.text()))
        self.control_panel_widget.set_open_prob(str(thr), str(oprob))
        if self.mediator.base.size:
            marks_time = self.find_intervals(self.mediator.time, self.mediator.var.compute_tvd(self.mediator.base[:, i]), thr)
            if marks_time and self.display_interv_state:
                ylims = self.mpl_widget.get_ylim()
                xlims = self.mpl_widget.get_xlim()
                marks_time = list(filter(lambda mtime: mtime > xlims[0] and mtime < xlims[1], marks_time))
                marks_vals = [ylims[0] + (ylims[1] - ylims[0])*0.03]*len(marks_time)
                #self.mpl_widget.plot_data(marks_time, marks_vals, color=[0, 0, 0], alpha=1.0, marker="^", markersize=10)
                for j in range(0, len(marks_time)-1, 2):
                #    print (i, len(marks_time)-1)
                    self.mpl_widget.plot_data([marks_time[j], marks_time[j+1]], [marks_vals[j], marks_vals[j+1]], color=[0, 0, 0])
        if self.mediator.base.size and self.display_tvd_state:
            self.mpl_widget.plot_data(self.mediator.time, self.mediator.var.compute_tvd(self.mediator.base[:, i]), self._data_color, float(self.control_panel_widget.transp_data_edit.text()))

    def update_style(self):
        data_color = self.control_panel_widget.style_data_edit.text()
        if data_color in colors:
            self._data_color = colors[data_color]
        sub_color = self.control_panel_widget.style_sub_edit.text()
        if sub_color in colors:
            self._sub_color = colors[sub_color]
        fit_color = self.control_panel_widget.style_fit_edit.text()
        if fit_color in colors:
            self._fit_color = colors[fit_color]
        base_color = self.control_panel_widget.style_base_edit.text()
        if base_color in colors:
            self._base_color = colors[base_color]
        thr_color = self.control_panel_widget.style_thr_edit.text()
        if thr_color in colors:
            self._thr_color = colors[thr_color]
        try:
            self.mpl_widget.font_size = int(self.control_panel_widget.style_font_edit.text())
        except Exception:
            pass
        self.update_mpl_widget()

    def fill_list(self, traces_count):
        self.traces_list_widget.traces_count = traces_count
        self.traces_list_widget.construct_traces_list()
        self.control_panel_widget.construct_tmed_list(traces_count)

    def change_display_tvd_state(self):
        if self.display_tvd_state:
            self.display_tvd_state = False
        else:
            self.display_tvd_state = True
        self.update_mpl_widget()

    def change_display_base_state(self):
        if self.display_fit_state:
            self.display_fit_state = False
        else:
            self.display_fit_state = True
        self.update_mpl_widget()

    def change_display_thr_state(self):
        if self.display_thr_state:
            self.display_thr_state = False
        else:
            self.display_thr_state = True
        self.update_mpl_widget()

    def change_display_interv_state(self):
        if self.display_interv_state:
            self.display_interv_state = False
        else:
            self.display_interv_state = True
        self.update_mpl_widget()

    def backup(self):
        self.mediator.backup()

    def save_results(self):
        self.mediator.res_folder = self.initialize_res_folder()
        self.mediator.save_results()
        self.mediator.calc_open_probability()
        self.mediator.write_time_constants()
        self.mediator.write_base_file()
        self.mediator.write_united_open()
        self.mediator.write_united_close()

    def send_to_log(self, log_string):
        self.log_widget.add_entry(log_string)

    def save_zoom_state(self):
        self.zoom_state = [self.mpl_widget.get_xlim(), self.mpl_widget.get_ylim()]

    def load_zoom_state(self):
        self.mpl_widget.set_xlim(self.zoom_state[0])
        self.mpl_widget.set_ylim(self.zoom_state[1])

    def get_size_window(self):
        return self.control_panel_widget.get_size_window()

    def get_remove_first_sec(self):
        return self.control_panel_widget.get_remove_first_sec()

    def get_open_prob_threshold(self):
        return self.control_panel_widget.get_open_prob_threshold()

    def set_open_prob_threshold(self, val):
        self.control_panel_widget.set_open_prob_threshold(val)

    def update_open_prob_threshold(self):
        try:
            val = self.control_panel_widget.get_open_prob_threshold()
            trace = self.traces_list_widget.button_group.checkedId()
            self.mediator.set_trace_open_prob_thr(trace, val)
            self.update_mpl_widget(thr_upd=False)
        except Exception:
            pass

    def find_intervals(self, time, data, thr):
        marks = []
        thr_on = False
        distance = 0.1
        for i in range(len(data)):
            #if time[i] < 5:
            #    print (time[i], data[i])
            if data[i] >= thr and not thr_on:
                #print (time[i], data[i])
                marks.append(time[i])
                thr_on = True
                #if len(marks) > 2:
                #    if time[i] > marks[-1]+distance:
                #        marks.append(time[i])
                #        thr_on = Truecorrection
                #else:
                #    marks.append(time[i])
                #    thr_on = True
            elif data[i] < thr and thr_on:
                marks.append(time[i])
                thr_on = False
                #if len(marks) > 2:
                #    if time[i] > marks[-1]+distance:
                #        marks.append(time[i])
                #        thr_on = False
                #else:
                #    marks.append(time[i])
                #    thr_on = False
        # filtered_marks = []
        if len(marks)%2 == 1:
            marks.append(time[-1])
        return marks

    def check_trace_state(self, i):
        return self.traces_list_widget.check_trace_state(i)

    def uncheck_trace_state(self, i):
        return self.traces_list_widget.set_trace_state(i, False)

if __name__ == '__main__':

    import sys

    app = QtWidgets.QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.mediator = Mediator()
    mainWin.mediator.main_window = mainWin
    mainWin.show()
    sys.exit(app.exec_())
