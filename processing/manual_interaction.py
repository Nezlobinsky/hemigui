#!/usr/bin/python2

import os
import matplotlib

# matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
import pdb
import argparse
import sys
from math import *
import pickle

from processing.tvd import tvd
from scipy.optimize import curve_fit
import scipy
import processing.general as g




def func(z, a, b, c):
    return a*(1 - np.exp(-(z)/b)) + c

def func2(z, a, b, c, d, e):
    return a*(1 - np.exp(-z/b)) + c *(1 - np.exp(-z/d))  + e

def func3(z, a, b, c):
    return a*np.log(z + c) + b

def func4(z, a, b):
    return a*z + b



class Action:
    def __call__(self, t):
        self.f(t)
    def change(self, g):
        self.f = g

class Data_variables:
    def __init__(self, fine_p, fine_n, tmax, data, data_base, log_string=""):
        self.size = 500
        self.tw = tmax # 0.150
        self.pos = fine_p
        self.neg = fine_n
        (self.jumps_p, self.jumps_n, self.rise_p, self.rise_n, string) = g.find_jumps_all(data, fine_p, fine_n, self.tw)
        log_string += string

        self.data = data
        self.sub = g.subtract_jumps(data, self.jumps_p, self.jumps_n, fine_p, fine_n) #data without jumps
        self.data_base = data_base #data_without_basline
        self.data_base_sub = data_base #data_without_baslineData_variabl
        self.fit = np.zeros((g.N,g.Ntr)) #the filn4e
        self.sigma = np.ones((g.N,g.Ntr)) #weights
        (self.bumps_p, self.bumps_n, self.bise_p, self.bise_n, string) = g.find_jumps_all(data_base, fine_p, fine_n, self.tw) #basejumps
        log_string += string

        # self.fig = plt.figure(figsize = (20,5))
        # self.action = Action()
        # self.action.change(self.change_sigma)
        self.key = 0
        self.switch = 0
        self.pn = "p" #to switch between pos en neg points
        self.fill_tvd()
        self.tvd_sw = 0
        # self.plot(True)

        self.log_string = log_string

    def fill_tvd(self):
        self.tvd = np.copy(self.data)
        for i in range(len(self.data[1,:])):
            try:
                self.tvd[:,i] = tvd(self.data[:,i], 2/g.dt)
            except Exception:
                pass

    def compute_tvd(self, data):
        tvd_data = np.copy(data)
        try:
            return tvd(tvd_data, 2/g.dt)
        except Exception:
            return []

    def next_key(self):
        k = (self.key + 1)%g.Ntr
        self.key = k

    def prev_key(self):
        k = (self.key - 1)%g.Ntr
        self.key = k

    def plot(self, first_time):
        dt = g.dt
        t = g.t
        n = self.key
        if(not first_time):
            axes = plt.gca()
            (yb, ye) = axes.get_ylim()
            (xb, xe) = axes.get_xlim()
        else:
            (xb, xe) = (0, len(self.data[:,n])*dt)
        self.fig.clf()
        pos = self.pos[n]
        neg = self.neg[n]
        y = np.multiply(self.data[:,n], self.sigma[:,n])
        y2 = np.multiply(self.data[:,n], (self.sigma[:,n] + 1)%2)
        if(self.tvd_sw == 1):
            plt.plot(t, self.tvd[:,n], color = 'cyan', zorder = 1)
        else:
            plt.plot(t, self.fit[:,n], color = 'blue', zorder = 3)
            plt.plot(t, y, color = 'cyan', zorder = 2)
            plt.plot(t, y2, color = 'gray', zorder = 2)
            plt.plot(t, self.sub[:,n], color = 'yellow', zorder = 1)
            plt.plot(t, self.data_base[:,n], color = 'darkgoldenrod', zorder = 0)
        val_p = [self.tvd[int(x), n] for x in pos]
        val_n = [self.tvd[int(x), n] for x in neg]
        bal_p = [self.data_base[int(x), n] for x in pos]
        bal_n = [self.data_base[int(x), n] for x in neg]
        ax = self.fig.add_subplot(111)
        abc_p=ax.scatter(np.multiply(pos,dt), val_p, s=50, color='red', zorder = 3)
        abc_n=ax.scatter(np.multiply(neg,dt), val_n, s=50, color='green', zorder = 3)
        if(self.tvd_sw == 0):
            abc_p=ax.scatter(np.multiply(pos,dt), bal_p, s=50, color='red', zorder = 3)
            abc_n=ax.scatter(np.multiply(neg,dt), bal_n, s=50, color='green', zorder = 3)
        for i in range(len(pos)):
            plt.text(pos[i]*dt, val_p[i] + 200, int(self.jumps_p[n][i]))
            if(self.tvd_sw == 0):
                plt.text(pos[i]*dt, bal_p[i] + 200, int(self.bumps_p[n][i]), color = 'red')
        for i in range(len(neg)):
            plt.text(neg[i]*dt, val_n[i] + 300, -int(self.jumps_n[n][i]))
            if(self.tvd_sw == 0):
                plt.text(neg[i]*dt, bal_n[i] + 300, -int(self.bumps_n[n][i]), color = 'red')
        axes = plt.gca()

        axes.set_xlim(xb, xe)
        if(not first_time):
           axes.set_ylim(yb, ye)
        self.fig.show()

    def update(self, trace):
        n = trace
        log_string = ""
        (self.jumps_p[n], self.jumps_n[n], self.rise_p[n], self.rise_n[n], string) = g.find_jumps_one_trace(self.data, self.pos, self.neg, self.tw, n)
        log_string += string
        self.sub = g.subtract_jumps(self.data, self.jumps_p, self.jumps_n, self.pos, self.neg )
        (self.bumps_p[n], self.bumps_n[n], self.bise_p[n], self.bise_n[n], string) = g.find_jumps_one_trace(self.data_base, self.pos, self.neg, self.tw, n)
        log_string += string

        return log_string

        # print("updated!")
        # self.log_string += "updated!/n"

    def give_fitdata(self):
        n = self.key
        #if self.switch == 0:
            # print("fit was done from original data")
        self.log_string += "fit was done from original data\n"
        return self.data[:,n]
        #else:
        #    # print("fit was done from data without jumps")
        #    self.log_string += "fit was done from data without jumps\n"
        #    return self.sub[:,n]

    def add_switch(self):
        self.switch = (self.switch + 1)%2
        if self.switch == 0:
            # print("fit will be done from original data")
            self.log_string += "fit will be done from original data\n"
        else:
            self.log_string += "fit will be done from data without jumps\n"
            # print("fit will be done from data without jumps")

    def change_sigma(self, t):
        num = int(t/g.dt)
        n = self.key
        self.sigma[num:,n] = (self.sigma[num:,n] + 1)%2
        self.plot(False)

    def sigma_reset(self):
        self.sigma[:,self.key] = 1
        self.plot(False)

    def give_sigma(self, trace, start, end):
        s = np.copy(self.sigma[:,trace][start:end])
        s[s==0] = 1e5
        return s

    def do_nothing(self,t):
        print("press a to append, x to append with tvd, d to delete. To switch press p or n, w for changing to tvd")


    def add_point(self, t):
        num = t/g.dt
        if t < 0:
            t = 0
        n = self.key
        if self.pn == 'p':
            self.pos[n].append(num)
            self.pos[n] = sorted(self.pos[n])
        if self.pn == 'n':
            self.neg[n].append(num)
            self.neg[n] = sorted(self.neg[n])
        self.update()
        self.plot(False)

    def add_point_der(self, n, t, pn, size):
        num = int(t/g.dt)
        if num < 0:
            num = 0
        print("you clicked t = {0} s".format(t))
        data = self.data[num - size: num + size, n]
        # data = self.data[:,n]
        data_tvd = tvd(data, 2/g.dt)
        der = np.diff(data_tvd)
        if pn == "p":
            t_n = np.argmax(der) + num - size
            print("the max (pos) was {0}".format(min(der)))
            print("t = {0} s was added".format(t_n*g.dt))
            self.pos[n].append(t_n)
            self.pos[n] = sorted(self.pos[n])
            self.update(n)
            return self.pos
        if pn == "n":
            t_n = np.argmin(der) + num - size
            print("the min (neg) was {0}".format(min(der)))
            print("t = {0} s was added".format(t_n*g.dt))
            self.neg[n].append(t_n)
            self.neg[n] = sorted(self.neg[n])
            self.update(n)
            return self.neg
        # self.plot(False)


    def delete_point(self, t):
        dt = g.dt
        n = self.key
        if self.pn == "p":
            for num in self.pos[n]:
                if abs(num*dt - t) < self.size * dt :
                    self.pos[n].remove(num)
                    print("{0} removed".format(num*dt))
                    break
        if self.pn == "n":
            for num in self.neg[n]:
                if abs(num*dt - t) < self.size * dt:
                    self.neg[n].remove(num)
                    print("{0} removed".format(num*dt))
                    break
        self.update()
        # self.plot(False)

    def extract_part(self, from_to):
        try:
            f, t = from_to.split("-")
            return float(f), float(t)
        except Exception:
            return 0, 0

    def fitexp(self, trace, from_to, popt):
        # print("exp fit")
        self.log_string += "exp fit\n"
        n = trace
        y = self.sub[:,n] # self.give_fitdata()[till:]
        a = y[g.N-1] - y[0]
        b = 1.0
        c = y[0]
        d = 0
        first, second = self.extract_part(from_to)
        if second == 0:
            start = 0
            end = len(y)
        else:
            start = int(first/g.dt)
            end   = int(second/g.dt)
        try:
            if popt is None:
                popt, pcov = curve_fit(func, g.t[start:end], y[start:end], p0=[a, b, c], sigma = self.give_sigma(trace, start, end))
            y2 = func(g.t, *popt)
            # plt.plot(y2)
            print(popt)
            self.fit[:,n] = y2
            self.data_base[:,n] = np.subtract(self.data[:,n], y2)
            # self.plot()
            return popt
        except:
            self.log_string += "Fit did not work\n"
            # print("Fit did not work {0}".format(sys.exc_info()[0]))

    def fitexp2(self, trace, from_to, popt):
        # print("double exp fit")
        #till = int(remove_first/g.dt)
        self.log_string += "double exp fit\n"
        n = trace
        y = self.sub[:,n] # self.give_fitdata()[till:]
        a = y[g.N-1] - y[0]
        b = 1.0
        c = a
        d = 20.0
        e = y[0]
        first, second = self.extract_part(from_to)
        if second == 0:
            start = 0
            end = len(y)
        else:
            start = int(first/g.dt)
            end   = int(second/g.dt)
        try:
            if popt is None:
                popt, pcov = curve_fit(func2, g.t[start:end], y[start:end], p0=[a, b, c, d, e], sigma = self.give_sigma(trace, start, end), bounds = ([0, 0, 0, 0, -10000], [a*2, 30, a*2, 2000, 10000]))
            y2 = func2(g.t, *popt)
            print(popt)
            self.fit[:,n] = y2
            self.data_base[:,n] = np.subtract(self.data[:,n], y2)
            # self.plot(True)
            return popt
        except:
            self.log_string += "Fit did not work\n"
            # print("Fit did not work {0}".format(sys.exc_info()[0]))

    def fitln(self, trace, from_to, popt):
        # print("log fit")
        #till = int(remove_first/g.dt)
        self.log_string += "log fit\n"
        n = trace
        y = self.sub[:,n] # self.give_fitdata()[till:]
        #y = y[1:]
        #try:
        first, second = self.extract_part(from_to)
        if second == 0:
            start = 0
            end = len(y)
        else:
            start = int(first/g.dt)
            end   = int(second/g.dt)
        if popt is None:
            popt, pcov = curve_fit(func3, g.t[start:end], y[start:end], p0 = [500, y[0], 0])
        y2 = func3(g.t, *popt)
        self.fit[:,n] = y2
        # plt.plot(y2)
        print(popt)
        self.data_base[:,n] = np.subtract(self.data[:, n], y2)
        return popt
        #self.data_base[1:,n] = np.subtract(self.data[:,n], y2)
        #self.data_base[0,n] = self.data_base[1,n]
        # self.plot(True)
        #except:
        #    print("Fit did not work {0}".format(sys.exc_info()[0]))

    def fitlin(self, trace, from_to, popt):
        # print("linear fit")
        #till = int(remove_first/g.dt)
        self.log_string += "linear fit\n"
        n = trace
        y = self.sub[:,n] #self.give_fitdata()[till:]
        #try:
        first, second = self.extract_part(from_to)
        if second == 0:
            start = 0
            end = len(y)
        else:
            start = int(first/g.dt)
            end   = int(second/g.dt)
        if popt is None:
            popt, pcov = curve_fit(func4, g.t[start:end], y[start:end], sigma = self.give_sigma(trace, start, end))
        y2 = func4(g.t, *popt)
        self.fit[:,n] = y2
        self.data_base[:,n] = np.subtract(self.data[:, n], y2)
        return popt
        # self.plot(True)
        #except:
        #print("Fit did not work {0}".format(sys.exc_info()[0]))

    def do_nothing(self,t):
        print("press a to append, x to append with tvd, d to delete. To switch press p or n, w for changing to tvd")

    def onclick(self,event):
        self.action(event.xdata)

    def on_key(self, event):
        if event.key == 'z':
            print("zoom out")
            self.plot(True)
        if event.key == 'right':
            self.next_key()
            print(self.key)
            self.plot(True)
        if event.key == 'left':
            self.prev_key()
            print(self.key)
            self.plot(True)
        if event.key == 'p':
            self.pn = 'p'
            print('you look at the positive jumps now')
        if event.key == 'n':
            self.pn = 'n'
            print('you look at the negative jumps now')
        if event.key == 'o':
            self.action.change(self.do_nothing)
            print('you can not change anything')
        if event.key == 'a':
            self.action.change(self.add_point)
            print('you can now add points')
        if event.key == 'x':
            self.action.change(self.add_point_der)
            print('you can now add points which will look for min of derivatives')
        if event.key == 'up':
            self.size = self.size + 5
            print('the window for x is now 2 * {0} s'.format(self.size *g.dt))
        if event.key == 'down':
            self.size = self.size - 5
            print('the window for x is now 2 * {0} s'.format(self.size*g.dt))
        if event.key == 'd':
            self.action.change(self.delete_point)
            print('you can now delete')
        if event.key == 'w':
            self.tvd_sw = (self.tvd_sw + 1)%2
            self.plot(False)
        if event.key == 'c':
            self.add_switch()
        if event.key == 'v':
            print("you can now add the weights")
            self.action.change(self.change_sigma)
        if event.key == 'r':
            self.sigma_reset()

        if event.key == '1':
            self.fitexp()
        if event.key == '2':
            self.fitexp2()
        if event.key == '3':
            self.fitln()
        if event.key == '4':
            self.fitlin()
        if event.key == '0':
            self.change_sigma(0)

def mk_parser():
    parser = argparse.ArgumentParser(
        description="Find all the activation times",
        epilog="Specify the file -f and the times -t1 -t2, make sure to also include some normal beats")
    parser.add_argument('-i', type=str, metavar='input',
                        help='give the input name')
    parser.add_argument('-o', type=str, metavar='data',
                        help='give the name where the pos en neg files are located')
    parser.add_argument("-n", help='just fill something', action = "store_true")
    return parser



def manual_interaction(fine_p, fine_n, data, data_base_name):
    # parser = mk_parser()
    # args = parser.parse_args()
    # if args.i != None and args.o != None:
    #     name_p = '{0}/fine_p.p'.format(args.o)
    #     name_n = '{0}/fine_n.p'.format(args.o)
    #     name_jp = '{0}/jumps_p.p'.format(args.o)
    #     name_jn = '{0}/jumps_n.p'.format(args.o)
    # else:
    #     parser.print_help()
    #     sys.exit(2)

    #loading the data
    # A = np.loadtxt(args.i)
    # data = g.init(A)
    # fine_p = pickle.load(open(name_p, "rb"))
    # fine_n = pickle.load(open(name_n, "rb"))
    #pdb.set_trace()
    try:
        data_base = np.loadtxt("{0}/2data_base.txt".format(args.o))
        print("data_base was loaded from file")
    except:
        data_base = np.copy(data)
    #data interaction
    var = Data_variables(fine_p, fine_n, data, data_base)
    cid = var.fig.canvas.mpl_connect('button_press_event', var.onclick)
    cid = var.fig.canvas.mpl_connect('key_press_event', var.on_key)
    plt.show()

    #save the results
    np.savetxt("{0}/2data_base.txt".format(args.o), var.data_base)
    print("The data base was written")
    pickle.dump( var.pos, open( name_p, "wb" ) )
    pickle.dump( var.neg, open( name_n, "wb" ) )
    name_p = '{0}/jumps_p.p'.format(args.o)
    name_n = '{0}/jumps_n.p'.format(args.o)
    pickle.dump( var.jumps_p, open( name_p, "wb" ) )
    pickle.dump( var.jumps_n, open( name_n, "wb" ) )

    name_p = '{0}/rise_p.p'.format(args.o)
    name_n = '{0}/rise_n.p'.format(args.o)
    pickle.dump( var.rise_p, open( name_p, "wb" ) )
    pickle.dump( var.rise_n, open( name_n, "wb" ) )

    #take_backup
    name_p = '{0}/fine_p.p.bak'.format(args.o)
    name_n = '{0}/fine_n.p.bak'.format(args.o)
    pickle.dump( var.pos, open( name_p, "wb" ) )
    pickle.dump( var.neg, open( name_n, "wb" ) )

    #plot_figures
    arr1 = g.make_array(var.jumps_p)
    arr2 = g.make_array(var.bumps_p)
    plt.hist(arr1, bins =  np.linspace(0.0, 1000.0, num=200))
    plt.hist(arr2, bins =  np.linspace(0.0, 1000.0, num=200))
    plt.show()

    arr3 = g.make_array(var.jumps_n)
    arr4 = g.make_array(var.bumps_n)
    plt.hist(arr3, bins =  np.linspace(0.0, 1000.0, num=200))
    plt.hist(arr4, bins =  np.linspace(0.0, 1000.0, num=200))
    plt.show()

    arr1.extend(arr3)
    arr2.extend(arr4)
    plt.hist(arr1, bins =  np.linspace(0.0, 1000.0, num=200))
    plt.hist(arr2, bins =  np.linspace(0.0, 1000.0, num=200))
    plt.show()

#    pdb.set_trace()
    # sys.exit(0)
