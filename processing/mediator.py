from processing.make_jumps import make_jumps_main, load_data
from processing.manual_interaction import Data_variables
import processing.general as g

import os
import json
import pickle
import numpy as np
import matplotlib.pyplot as plt


class Mediator:
    def __init__(self):
        self.main_window = None

        self.data_file = None

        self.data_output = ""

        self.res_folder    = ""
        self.backup_folder = "backup"

        self.data  = np.array([])
        self.tvd   = np.array([])
        self.fit   = np.array([])
        self.sub   = np.array([])
        self.base  = np.array([])
        self.time  = np.array([])
        self.sigma = np.array([])

        self.base_file_name = ""
        self.var = None

        self.fine_p = {}
        self.fine_n = {}

        self.jumps_p = []
        self.jumps_n = []
        self.rise_p  = []
        self.rise_n  = []
        self.bumps_p = []
        self.bumps_n = []
        self.bise_p  = []
        self.bise_n  = []

        self.fit_parameters = []

        self.open_prob_thr = []
        self.open_prob_val = []
        self.open_prob_thr_states = []

    def load_data_mediator(self):
        self.reset_state()
        self.data, self.time = load_data(self.data_file)
        self.fit_parameters = [["", None, "0 - 0"] for i in range(len(self.data[0]))]
        self.open_prob_thr = [0]*len(self.data[0])
        self.open_prob_val = [0]*len(self.data[0])
        self.open_prob_thr_states = [0]*len(self.data[0])

    def reset_state(self):
        self.data  = np.array([])
        self.tvd   = np.array([])
        self.fit   = np.array([])
        self.sub   = np.array([])
        self.base  = np.array([])
        self.time  = np.array([])
        self.sigma = np.array([])

        self.fine_p = {}
        self.fine_n = {}

        self.jumps_p = []
        self.jumps_n = []
        self.rise_p  = []
        self.rise_n  = []
        self.bumps_p = []
        self.bumps_n = []
        self.bise_p  = []
        self.bise_n  = []

    def load_state(self, backup_dir, tmax):
        self.fine_n = pickle.load(open(os.path.join(backup_dir, "fine_n.p"), "rb"))
        self.fine_p = pickle.load(open(os.path.join(backup_dir, "fine_p.p"), "rb"))

        fitting_file = os.path.join(backup_dir, "fitting.p")
        if os.path.exists(fitting_file):
            self.fit_parameters = pickle.load(open(fitting_file, "rb"))

        self.manual_interaction(tmax)

        open_prob_thr_file = os.path.join(backup_dir, "threshold.p")
        if os.path.exists(open_prob_thr_file):
            self.open_prob_thr = pickle.load(open(open_prob_thr_file, "rb"))
            self.restore_open_prob_thr()

        states_file = os.path.join(backup_dir, "trace_states.p")
        if os.path.exists(states_file):
            states = pickle.load(open(states_file, "rb"))
            for i in range(len(states)):
                if states[i]:
                    self.main_window.check_trace_state(i)
                else:
                    self.main_window.uncheck_trace_state(i)

        self.restore_fit()

        self.main_window.send_to_log("State was loaded\n")

    def run_make_jumps(self, t_med, thr, sec, tmax):
        self.fine_p, self.fine_n, log_string = make_jumps_main(np.copy(self.data), self.data_output, t_med, thr, sec)
        self.main_window.send_to_log(log_string)

        self.manual_interaction(tmax)

    def open_prob(self, trace):
        return len(self.time[self.base[:, trace] > self.open_prob_thr[trace]])/len(self.time)

    def fill_open_prob_thr(self):
        Ntr = len(self.open_prob_thr_states)
        for i in range(Ntr):
            if self.open_prob_thr_states[i]:
                continue
            self.open_prob_thr[i] = self.select_cluster(i)
            self.open_prob_val[i] = self.open_prob(i) # len(self.time[self.base[:, i] > self.open_prob_thr[i]])/len(self.time)

    def select_cluster(self, trace):
        settings = json.load(open("settings.json"))
        clusters = settings["clusters"]
        level    = settings["cluster_lvl"]
        jumps = []
        if self.fine_p[trace]:
            jumps_p = list(self.data[:, trace][np.array(self.fine_p[trace]).astype(int)])
            jumps_p = np.array(jumps_p)
            jumps_p -= self.sub[:, trace][np.array(self.fine_p[trace]).astype(int)]
            jumps_p = list(jumps_p)
            jumps.extend(jumps_p)
        if self.fine_n[trace]:
            jumps_n = list(self.data[:, trace][np.array(self.fine_n[trace]).astype(int)])
            jumps_n = np.array(jumps_n)
            jumps_n -= self.sub[:, trace][np.array(self.fine_n[trace]).astype(int)]
            jumps_n = list(jumps_n)
            jumps.extend(jumps_n)
        if jumps:
            cluster_step = (max(jumps) - min(jumps))/clusters
            return min(jumps) + cluster_step*level + cluster_step/2.0
        return 0

    def select_original_cluster(self, trace):
        settings = json.load(open("settings.json"))
        clusters = settings["clusters"]
        level    = settings["cluster_lvl"]
        jumps = []
        if not (self.fine_p or self.fine_n):
            return 0
        if self.fine_p[trace]:
            jumps_p = list(self.data[:, trace][np.array(self.fine_p[trace]).astype(int)])
            jumps.extend(jumps_p)
        if self.fine_n[trace]:
            jumps_n = list(self.data[:, trace][np.array(self.fine_n[trace]).astype(int)])
            jumps.extend(jumps_n)
        if jumps:
            cluster_step = (max(jumps) - min(jumps))/clusters
            return min(jumps) + cluster_step*level + cluster_step/2.0
        return 0

    def get_original_threshold(self, trace, thr):
        thr_org = self.select_original_cluster(trace)
        if not self.correction[trace]:
            self.correction[trace] = thr_org - thr
        else:
            thr_org = thr + self.correction[trace]
        return thr_org

    def restore_open_prob_thr(self):
        Ntr = len(self.open_prob_thr)
        for i in range(Ntr):
            if self.open_prob_thr[i] == 0:
                continue
            self.open_prob_thr_states[i] = 1
            self.open_prob_val[i] = self.open_prob(i) # len(self.time[self.base[:, i] > self.open_prob_thr[i]])/len(self.time)

    def get_trace_open_prob_thr(self, trace):
        return self.open_prob_thr[trace]

    def get_trace_open_prob_val(self, trace):
        return self.open_prob_val[trace]

    def set_trace_open_prob_thr(self, trace, thr):
        self.open_prob_thr[trace] = thr
        self.open_prob_thr_states[trace] = 1
        self.open_prob_val[trace] = len(self.time[self.base[:, trace] > thr])/len(self.time)

    def add_positive(self, trace, coords):
        if trace in self.fine_p:
            t, c = coords
            ind = np.argmin(np.abs(t - self.time))
            size = self.main_window.get_size_window()
            pos = self.var.add_point_der(trace, t, "p", size)
            self.var.pos = pos
            self.update(trace)

    def del_positive(self, trace, coords):
        try:
            if trace in self.fine_p and self.fine_p[trace]:
                coords_arr = np.array([self.time, self.data[:, trace]]).T
                ind = np.argmin(np.sum((coords_arr - np.array(coords))**2, axis=1))
                nearest = np.argmin(np.abs(ind - self.fine_p[trace]))
                del self.fine_p[trace][nearest]
                self.fine_p[trace].sort()
                self.var.pos = self.fine_p
                self.update(trace)
        except Exception:
            self.main_window.send_to_log("Failed to delete the point. Please, try again.\n")

    def add_negative(self, trace, coords):
        if trace in self.fine_n:
            t, c = coords
            ind = np.argmin(np.abs(t - self.time))
            size = self.main_window.get_size_window()
            neg = self.var.add_point_der(trace, t, "n", size)
            self.var.neg = neg
            self.update(trace)

    def del_negative(self, trace, coords):
        try:
            if trace in self.fine_n and self.fine_n[trace]:
                coords_arr = np.array([self.time, self.data[:, trace]]).T
                ind = np.argmin(np.sum((coords_arr - np.array(coords))**2, axis=1))
                nearest = np.argmin(np.abs(ind - self.fine_n[trace]))
                del self.fine_n[trace][nearest]
                self.fine_n[trace].sort()
                self.var.neg = self.fine_n
                self.update(trace)
        except Exception:
            self.main_window.send_to_log("Failed to delete the point. Please, try again.\n")

    def manual_interaction(self, tmax):
        log_string = ""
        try:
            data_base = np.loadtxt(self.base_file_name)
            # print("data_base was loaded from file")
            log_string += "data_base was loaded from file\n"
        except:
            data_base = np.copy(self.data)
        #data interaction
        self.var = Data_variables(self.fine_p, self.fine_n, tmax, np.copy(self.data), data_base, log_string)

        self.tvd   = self.var.tvd
        self.fit   = self.var.fit
        self.sub   = self.var.sub
        self.base  = self.var.data_base
        self.sigma = self.var.sigma
        self.jumps_p = self.var.jumps_p
        self.jumps_n = self.var.jumps_n
        self.rise_p  = self.var.rise_p
        self.rise_n  = self.var.rise_n
        self.bumps_p = self.var.bumps_p
        self.bumps_n = self.var.bumps_n
        self.bise_p  = self.var.bise_p
        self.bise_n  = self.var.bise_n

        self.fill_open_prob_thr()

        self.main_window.send_to_log(self.var.log_string)

    def update(self, trace, send=True):
        log_string = self.var.update(trace)

        self.tvd   = self.var.tvd
        self.fit   = self.var.fit
        self.sub   = self.var.sub
        self.base  = self.var.data_base
        self.sigma = self.var.sigma
        self.jumps_p = self.var.jumps_p
        self.jumps_n = self.var.jumps_n
        self.rise_p  = self.var.rise_p
        self.rise_n  = self.var.rise_n
        self.bumps_p = self.var.bumps_p
        self.bumps_n = self.var.bumps_n
        self.bise_p  = self.var.bise_p
        self.bise_n  = self.var.bise_n

        self.fill_open_prob_thr()

        if send:
            self.main_window.send_to_log(log_string + "updated!\n")

    def fitexp(self, trace):
        try:
            self.fit_parameters[trace] = ["exp2", None, 0]
            self.fit_parameters[trace][1] = self.var.fitexp(trace, self.main_window.get_remove_first_sec(), None)
            self.fit_parameters[trace][2] = self.main_window.get_remove_first_sec()
            self.update(trace)
        except RuntimeError:
            self.main_window.send_to_log("There is a problem with the fitexp. Try to use another option.\n")

    def fitexp_all(self, trace):
        Ntr = len(self.sub[0])
        for i in range(Ntr):
            self.fitexp(i)
        self.main_window.send_to_log("All traces fit - done.\n")

    def fitexp2(self, trace):
        try:
            self.fit_parameters[trace] = ["exp2", None, 0]
            self.fit_parameters[trace][1] = self.var.fitexp2(trace, self.main_window.get_remove_first_sec(), None)
            self.fit_parameters[trace][2] = self.main_window.get_remove_first_sec()
            self.update(trace)
        except RuntimeError:
            self.main_window.send_to_log("There is a problem with the fitexp2. Try to use another option.\n")

    def fitexp2_all(self, trace):
        Ntr = len(self.sub[0])
        for i in range(Ntr):
            self.fitexp2(i)
        self.main_window.send_to_log("All traces fit - done.\n")

    def fitln(self, trace):
        try:
            self.fit_parameters[trace] = ["ln", None, 0]
            self.fit_parameters[trace][1] = self.var.fitln(trace, self.main_window.get_remove_first_sec(), None)
            self.fit_parameters[trace][2] = self.main_window.get_remove_first_sec()
            self.update(trace)
        except RuntimeError:
            self.main_window.send_to_log("There is a problem with the fitln. Try to use another option.\n")

    def fitln_all(self, trace):
        Ntr = len(self.sub[0])
        for i in range(Ntr):
            self.fitln(i)
        self.main_window.send_to_log("All traces fit - done.\n")

    def fitlin(self, trace):
        try:
            self.fit_parameters[trace] = ["lin", None, 0]
            self.fit_parameters[trace][1] = self.var.fitlin(trace, self.main_window.get_remove_first_sec(), None)
            self.fit_parameters[trace][2] = self.main_window.get_remove_first_sec()
            self.update(trace)
        except RuntimeError:
            self.main_window.send_to_log("There is a problem with the fitlin. Try to use another option.\n")

    def fitlin_all(self, trace):
        Ntr = len(self.sub[0])
        for i in range(Ntr):
            self.fitlin(i)
        self.main_window.send_to_log("All traces fit - done.\n")

    def restore_fit(self):
        Ntr = len(self.data[0])
        for i in range(Ntr):
            try:
                if self.fit_parameters[i][0] == "exp":
                    self.var.fitexp(i, self.fit_parameters[i][2], self.fit_parameters[i][1])
                elif self.fit_parameters[i][0] == "exp2":
                    self.var.fitexp2(i, self.fit_parameters[i][2], self.fit_parameters[i][1])
                elif self.fit_parameters[i][0] == "lin":
                    self.var.fitlin(i, self.fit_parameters[i][2], self.fit_parameters[i][1])
                elif self.fit_parameters[i][0] == "ln":
                    self.var.fitln(i, self.fit_parameters[i][2], self.fit_parameters[i][1])
                else:
                    continue
                self.update(i, send=False)
            except RuntimeError:
                self.main_window.send_to_log("There is a problem with the fit recovering for the trace " + str(i) + "\n")

    def restore_fit_state(self, i, fit_name, fit_func):
        if self.fit_parameters[i][0] == fit_name:
            self.fit_parameters[i][1] = fit_func(i, self.fit_parameters[i][2], self.fit_parameters[i][1])
        else:
            self.fit_parameters[i] = [fit_name, None, 0]
            self.fit_parameters[i][1] = fit_func(i, 0, None)

    def save_results(self):
        try:
            jumps_p = {}
            for key in self.jumps_p:
                if self.main_window.check_trace_state(key):
                    jumps_p[key] = self.jumps_p[key]
                else:
                    jumps_p[key] = []
            bumps_p = {}
            for key in self.bumps_p:
                if self.main_window.check_trace_state(key):
                    bumps_p[key] = self.bumps_p[key]
                else:
                    bumps_p[key] = []
            arr1 = g.make_array(jumps_p)
            arr2 = g.make_array(bumps_p)
            #plt.hist(arr1, color="b", bins =  np.linspace(0.0, 1000.0, num=200))
            plt.hist(arr2, color="m",bins =  np.linspace(0.0, 1000.0, num=200))
            plt.savefig(os.path.join(self.res_folder, "jumps_p_hist.png"), dpi=600)

            np.savetxt(os.path.join(self.res_folder, "jumps_p_hist.txt"), arr2)

            plt.close()

            jumps_n = {}
            for key in self.jumps_n:
                if self.main_window.check_trace_state(key):
                    jumps_n[key] = self.jumps_n[key]
                else:
                    jumps_n[key] = []
            bumps_n = {}
            for key in self.bumps_n:
                if self.main_window.check_trace_state(key):
                    bumps_n[key] = self.bumps_n[key]
                else:
                    bumps_n[key] = []
            arr3 = g.make_array(jumps_n)
            arr4 = g.make_array(bumps_n)
            #plt.hist(arr3, color="r", bins =  np.linspace(0.0, 1000.0, num=200))
            plt.hist(arr4, color="c", bins =  np.linspace(0.0, 1000.0, num=200))
            plt.savefig(os.path.join(self.res_folder, "jumps_n_hist.png"), dpi=600)

            np.savetxt(os.path.join(self.res_folder, "jumps_n_hist.txt"), arr4)

            plt.close()

            arr1.extend(arr3)
            arr2.extend(arr4)
            #plt.hist(arr1, color="y", bins =  np.linspace(0.0, 1000.0, num=200))
            plt.hist(arr2, color=[1, 0.5, 0], bins =  np.linspace(0.0, 1000.0, num=200))
            plt.savefig(os.path.join(self.res_folder, "jumps_n_p_hist.png"), dpi=600)

            np.savetxt(os.path.join(self.res_folder, "jumps_n_p_hist.txt"), arr2)

            plt.close()

            self.main_window.send_to_log("The results were written\n")
        except Exception:
            self.main_window.send_to_log("Can't write the results. Please, complete processing and try again (or run the software again)\n")

    def calc_open_probability(self):
        with open(os.path.join(self.res_folder, "open_probability.txt"), "w") as oprob:
            Ntr = len(self.data[0])
            for i in range(Ntr):
                if self.main_window.check_trace_state(i):
                    thr = self.get_trace_open_prob_thr(i)
                    oprob.write(str(i) + " " + str(len(self.time[self.base[:, i] > thr])/len(self.time)) + "\n")

    def write_time_constants(self):
        with open(os.path.join(self.res_folder, "open_time_constants.txt"), "w") as topen:
            Ntr = len(self.data[0])
            for i in range(Ntr):
                if self.main_window.check_trace_state(i):
                    open_t = self.rise_p[i]
                    topen.write(str(i))
                    for t in open_t:
                        topen.write(" " + str(t))
                    topen.write("\n")

        with open(os.path.join(self.res_folder, "close_time_constants.txt"), "w") as tclose:
            Ntr = len(self.data[0])
            for i in range(Ntr):
                if self.main_window.check_trace_state(i):
                    close_t = self.rise_n[i]
                    tclose.write(str(i))
                    for t in close_t:
                        tclose.write(" " + str(t))
                    tclose.write("\n")

    def write_united_open(self):
        with open(os.path.join(self.res_folder, "united_open.txt"), "w") as united_open:
            Ntr = len(self.data[0])
            for i in range(Ntr):
                if self.main_window.check_trace_state(i):
                    united_open.write(str(i))
                    open_t_ls = self.bumps_p[i]
                    if not open_t_ls:
                        united_open.write("\n")
                    for j in range(len(open_t_ls)):
                        jump_v = self.bumps_p[i][j]
                        open_t = self.rise_p[i][j]
                        united_open.write("\t" + str(open_t) + "\t" + str(jump_v) + "\n")

    def write_united_close(self):
        with open(os.path.join(self.res_folder, "united_close.txt"), "w") as united_close:
            Ntr = len(self.data[0])
            for i in range(Ntr):
                if self.main_window.check_trace_state(i):
                    united_close.write(str(i))
                    close_t_ls = self.bumps_n[i]
                    if not close_t_ls:
                        united_close.write("\n")
                    for j in range(len(close_t_ls)):
                        jump_v = self.bumps_n[i][j]
                        close_t = self.rise_n[i][j]
                        united_close.write("\t" + str(close_t) + "\t" + str(jump_v) + "\n")            
                    

    def write_base_file(self):
        np.savetxt(os.path.join(self.res_folder, "corrected_base.txt"), np.vstack((self.time, self.base.T)).T)

    def backup(self):
        if not os.path.exists(self.backup_folder) and self.backup_folder:
            os.makedirs(self.backup_folder)
        #np.savetxt(os.path.join(self.backup_folder, "data_base.txt"), self.base)
        name_p = os.path.join(self.backup_folder,'fine_p.p')
        name_n = os.path.join(self.backup_folder,'fine_n.p')
        pickle.dump( self.fine_p, open( name_p, "wb" ) )
        pickle.dump( self.fine_n, open( name_n, "wb" ) )

        fit_name = os.path.join(self.backup_folder, 'fitting.p')
        pickle.dump(self.fit_parameters, open(fit_name, "wb"))

        thr_name = os.path.join(self.backup_folder, "threshold.p")
        pickle.dump(self.open_prob_thr, open(thr_name, "wb"))

        states = []
        Ntr = len(self.data[0])
        for i in range(Ntr):
            states.append(self.main_window.check_trace_state(i))
        states_name = os.path.join(self.backup_folder, "trace_states.p")
        pickle.dump(states, open(states_name, "wb"))

        self.main_window.send_to_log("The data base was written\n")
