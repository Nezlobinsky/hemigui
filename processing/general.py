#!/usr/bin/python3

import os
import matplotlib.pyplot as plt
import numpy as np
import pdb
import argparse
import sys
from math import *
import pickle
from scipy.signal import argrelmin
from scipy.optimize import leastsq
from processing.tvd import tvd
from scipy.optimize import curve_fit
import scipy

dt = 0
Ntr = 0
N = 0
t = None



def init(data):
    global N, Ntr, t, dt
    N = len(data)
    Ntr = len(data[0]) - 1
    t = data[:,0]
    dt = (t[N-1] - t[0])/N
    return data[:,1:]


def subtract_jumps(data, jumps_p, jumps_n, fine_p, fine_n):
    data_sub = np.copy(data)
    for n in range(Ntr):
        for i in range(len(jumps_p[n])):
            p = int(fine_p[n][i])
            j = jumps_p[n][i]
            data_sub[p:, n] = data_sub[p:,n] - j
        for i in range(len(jumps_n[n])):
            p = int(fine_n[n][i])
            j = jumps_n[n][i]
            data_sub[p:, n] = data_sub[p:,n] +  j
    return data_sub


def func(z, a, b, c):
    return a*scipy.special.erf(z/b) + c

def func2(z, a, b, c):
    return a*scipy.special.erf(z*b) + c

def residual(vars, x, data):
    a = vars[0]
    b = vars[1]
    c = vars[2]
    model = a*scipy.special.erf(x/b) + c
    return (model - data)

def residual2(vars, x, data):
    a = vars[0]
    b = vars[1]
    c = vars[2]
    model = a*scipy.special.erf(x*b) + c
    return (model - data)


def find_boundaries(p, fine_pi, fine_ni, twn):
    xl = max(p - twn, 0)
    xr = min(p + twn, N-1)
    for pf in fine_pi:
        if pf > xl and pf < p:
            xl = pf
        if pf < xr and pf > p:
            xr = pf
    for pn in fine_ni:
        if pn > xl and pn < p:
            xl = pn
        if pn < xr and pn > p:
            xr = pn
    return (int(xl), int(xr))


def jump(p, fine_pi, fine_ni, twn, data, i, pn):
    log_string = ""
    (xl, xr) = find_boundaries(p, fine_pi, fine_ni, twn)
    x = np.linspace(xl - int(p),xr - int(p),xr-xl)
    y = data[xl:xr]
    try:
        #test1
        vars = [200, 0.01/dt, x[0]]
        test = leastsq(residual, vars, args = (x,y), full_output=True, ftol=1.0e-10)
        popt = test[0]
        y2 = func(x, *popt)
        jump = popt[0]*2
        rise = popt[1]*dt*1000
        shift = abs(popt[2])
        #print('trace {0}: j = {1} r = {2:.9f}  at t = {3:.0f}, shift {4:.2f}'.format( i, int(jump), rise, p *dt, shift))

        if abs(jump) > 1000:
            popt2, pcov2 = curve_fit(func2, x, y)
            y2p = func2(x, *popt2)
            jump2 = popt2[0]*2
            rise2 = 1/popt2[1]*dt*1000
            #print('new fit! trace {0}: j = {1} r = {2}  at t = {3}'.format( i, int(jump2), rise2, p *dt))
            #plt.figure(2)
            #plt.plot(x,y)
            #plt.plot(x, y2, label = 'z/b')
            #plt.plot(x, y2p, label = 'z*b')
            #plt.legend()
            #plt.show()
            if abs(jump2) < 1000:
                return (abs(jump2), abs(rise2), log_string)
            else:
                log_string += "There is a problem at trace {0} pos {1}.\n".format(i,p)
                # print('There is a problem at trace {0} pos {1}'.format(i,p))
                return (0,0, log_string)

        return (abs(jump), abs(rise), log_string)
    except:
        log_string += "There is a strange problem at trace {0} pos {1}.\n".format(i,p)
        # print('There is a strange problem at trace {0} pos {1}'.format(i,p))
        return (0, 0, log_string)


def find_jumps_one_trace(data, fine_p, fine_n, tw, i):
    twn = int(tw/(2*dt))
    jumps_p = []
    jumps_n = []
    rise_p = []
    rise_n = []
    log_string = ""
    for p in fine_p[i]:
        #print('pos')
        j, tn, string = jump(p, fine_p[i], fine_n[i], twn, data[:,i], i, 1)
        log_string += string

        jumps_p.extend([j])
        rise_p.extend([tn])
    for n in fine_n[i]:
        #print('neg')
        j, tn, string = jump(n, fine_p[i], fine_n[i], twn, data[:,i], i, -1)
        log_string += string
        jumps_n.extend([j])
        rise_n.extend([tn])
    return (jumps_p, jumps_n, rise_p, rise_n, log_string)


def find_jumps_all(data, fine_p, fine_n, tw):
    jumps_p = {}
    jumps_n = {}
    rise_p = {}
    rise_n = {}
    log_string = ""
    for i in range(Ntr):
        (jumps_p[i], jumps_n[i], rise_p[i], rise_n[i], string) = find_jumps_one_trace(data, fine_p, fine_n, tw, i)
        log_string += string
    return (jumps_p, jumps_n, rise_p, rise_n, log_string)


def make_array(jumps):
    arr = []
    for i in range(Ntr):
        arr.extend(jumps[i])
    return arr


def make_array_size_thr(size, jumps, thr1, thr2):
    #pdb.set_trace()
    arr = []
    for i in range(Ntr):
        for n in range(len(jumps[i])):
            if jumps[i][n] > thr1 and jumps[i][n] < thr2:
                arr.append(size[i][n])
    return arr


def make_all_point_hist(data_base, s, name):
    arr = []
    print(dt)
    start = ceil(s/dt)
    print("We start from measure point {0}".format(start))
    fig = plt.figure(figsize = (10,10))
    for i in range(Ntr):
        arr.extend(data_base[start:,i])
    plt.hist(arr, bins =  np.linspace(-500, 1000.0, num=1000))
    plt.savefig(name)
    #plt.show()

def initialize_pos():
    fine_p = {}
    fine_n = {}
    for n in range(Ntr):
        fine_p[n] = []
        fine_n[n] = []
    return (fine_p, fine_n)
