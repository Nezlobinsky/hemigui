import numpy as np
from scipy.integrate import simps


def open_probability(time, data, jumps_p, jumps_n, thr):
    return len(time[data > thr])/len(time)
