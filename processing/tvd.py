#!/usr/bin/python3
import numpy as np
from scipy.linalg import solve_banded

def difft(x):
    return - np.ediff1d(x, to_begin=x[0], to_end=-x[-1])

def dist2(x, y):
    return sum((x - y) ** 2)

def norm1(x):
    return sum(abs(x))

def tridiag(ab, b):
    return solve_banded((1, 1), ab, b, overwrite_ab=True)

class TVD:
    def __init__(self, y, lam):
        self.x = y
        self.y = y
        self.dy = np.diff(y)
        self.lam = lam
        self.mat = np.ones((3, len(y) - 1))

    def advance(self, n=1):
        for i in range(n):
            self.mat.fill(-1.0)
            self.mat[1,:] = 1.0 / self.lam * abs(np.diff(self.x)) + 2.0
            self.x = self.y - difft(tridiag(self.mat, self.dy))
        return self.x

    def cost(self):
        return 0.5 * dist2(self.x, self.y) + self.lam * norm1(np.diff(self.x))

def plot(x):
    gp = os.popen('/usr/bin/gnuplot --persist', 'w')
    gp.write("plot '-' with lines\n")
    for el in x:
        gp.write("%f\n" % el)
    gp.write("e\n")
    gp.close()

class TVDnotConverge(Exception):
    pass

def tvd(signal, smoothing, tol=1e-6, max_iter=100):
    """
    Denoise the signal using the TVDmm method

    Parameters
    ----------
    signal : (N,) array_like
        A signal to denoise.
    smoothing : float
        Regularization parameter, higher values of `smoothing` result in
        more aggressive denoising
    tol : float, optional
        Tolerance for checking whether the method converged
    max_iter : int, optional
        Maximum number of iteragtions

    Returns
    -------
    tvd : (N,) array_like
        Filtered signal

    References
    ----------
    .. [1] "On total-variation denoising: A new majorization-minimization
            algorithm and an experimental comparison with wavalet denoising",
           M. Figueiredo, J. Bioucas-Dias, J. P. Oliveira, and R. D. Nowak.
           Proc. IEEE Int. Conf. Image Processing, 2006.
    .. [2] Ivan Selesnick, Lecture Notes on the TVDmm Algorithm
           http://eeweb.poly.edu/iselesni/lecture_notes/TVDmm/
    """
    if np.isnan(signal).any() or np.isinf(signal).any():
        raise TVDnotConverge
    proc = TVD(signal, smoothing)
    cost = proc.cost()
    b = int(max_iter/10)
    for i in range(b):
        proc.advance(n = 10)
        c = proc.cost()
        if (c - cost) / len(signal) < tol:
            return proc.x
    raise TVDnotConverge

# Testing

def gen_signal():
    e = lambda x: np.ones(x)
    o = lambda x: np.zeros(x)
    segs = [o(25), 4 * e(10), -1 * e(5), 2 * e(25), -2 * e(5), 3 * e(30),
            -1 * e(10), e(60), 5 * e(20), 2 * e(5), 3.5 * e(10), o(40)]
    return np.concatenate(segs)

def noise(signal, std):
    return (signal + std * np.random.randn(len(signal)))

if __name__ == '__main__':
    from math import sqrt
    orig = gen_signal()
    filt = tvd(noise(orig, 0.5), 1.0)
    rmse = sqrt(dist2(orig, filt)) / len(orig)
    print ("RMSE = {}".format(rmse))
