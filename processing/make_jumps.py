#!/usr/bin/python2

import os
import matplotlib.pyplot as plt
import numpy as np
import pdb
import argparse
import sys
from math import *
from scipy import signal
from processing.tvd import tvd
import pickle
import processing.general as g

sec = 0

def mk_parser():
    parser = argparse.ArgumentParser(
        description="Find all the activation times")
    parser.add_argument('-i', type=str, metavar='input',
                        help='give the input name')
    parser.add_argument('-o', type=str, metavar='output',
                        help='give the output folder')
    parser.add_argument('-s', type=float, metavar='output',
                        help='give s which should be cut')

    return parser


#median_filter of the data
#t_med is the time for the filtering filtered
def median_filter(data, t_med):
    N_med = int(ceil(t_med/g.dt))
    med = np.copy(data)
    num = int(ceil(g.N/N_med))
    for k in range(num + 1):
        x1 = k * N_med
        x2 = min((k+1) * N_med, g.N)
        med[x1:x2,:] = np.median(data[x1:x2,:], axis =0)
    return med

def find_jumps_med(data_med, thr):
    # print("median filter at work")
    jumps_p = {}
    jumps_n = {}
    sw = 0
    for n in range(g.Ntr):
        jumps_p[n] = []
        jumps_n[n] = []
        der = np.diff(data_med[:,n])
        for t in range(g.N - 1):
            if sw == 0 and abs(der[t]) > thr:
                if der[t] > 0:
                    jumps_p[n].extend([t])
                else:
                    jumps_n[n].extend([t])
                sw = 1
            elif sw == 1 and abs(der[t]) < thr:
                sw = 0
    # print(jumps_p, jumps_n)
    return (jumps_p, jumps_n)

def finetune_jumps_tvd(jumps_p, jumps_n, data, t_med):
    # print("finetune jumps")
    fine_p = {}
    fine_n = {}
    window = int(t_med/g.dt + 50)

    for n in range(g.Ntr):
        # print(n)
        data_tvd = tvd(data[:,n], 10/g.dt)
        der = np.diff(data_tvd)

        pos_p = jumps_p[n]
        s = set()
        for t in pos_p:
            if t > (sec/g.dt):
                x1 = max(0, t - window)
                x2 = min(g.N-2, t + window)
                x = np.argmax(der[x1:x2]) + x1
                s.add(x)
        fine_p[n] = sorted(list(s))

        pos_n = jumps_n[n]
        s = set()
        for t in pos_n:
            if t > (sec/g.dt):
                x1 = max(0, t - window)
                x2 = min(g.N-2, t + window)
                x = np.argmin(der[x1:x2]) + x1
                s.add(x)
        fine_n[n] = sorted(list(s))

    return (fine_p, fine_n)

def plot(jumps_p, jumps_n, data, n):
    plt.plot(data[:,n])
    dot_p = jumps_p[n]
    val_p = [data[x,n] for x in dot_p]
    dot_n = jumps_n[n]
    val_n = [data[x,n] for x in dot_n]
    plt.plot(dot_p, val_p, 'ro')
    plt.plot(dot_n, val_n, 'go')
    plt.show()

def load_data(data_file):
    A = np.loadtxt(data_file)
    data = g.init(A)
    time = A[:, 0]
    return data, time

def make_jumps_main(data, data_output, t_med, thr, sec_, log_string=""):
    global sec
    sec = sec_
    # parser = mk_parser()
    # args = parser.parse_args()
    # if args.i != None and args.o != None:
    #     input = args.i
    #     output = args.o
    # else:
    #     parser.print_help()
    #     sys.exit(2)
    # if args.s != None:
    #     sec = args.s


    # load the data
    # A = np.loadtxt(data_input)
    # data = g.init(A)
    # print(g.dt)
    #parameters program
    # t_med = 0.300 #s
    # thr = 60

    #finding jumps ad storing them
    data_med = median_filter(data, t_med)
    log_string += "Median filter at work...\n"
    (jumps_p, jumps_n) = find_jumps_med(data_med, thr)

    (fine_p, fine_n) = finetune_jumps_tvd(jumps_p, jumps_n, data, t_med)
    log_string += "Finetune jumps...\n"
    return fine_p, fine_n, log_string
    # name = '{0}/fine_p.p'.format(output)
    # pickle.dump(fine_p, open( name, "wb" ) )
    # name = '{0}/fine_n.p'.format(output)
    # pickle.dump(fine_n, open( name, "wb" ) )


    # sys.exit(0)

if __name__ == '__main__':
    make_jumps_main()
