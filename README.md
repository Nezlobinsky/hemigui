### 1. Install anaconda distriburion (optional*)
See https://docs.anaconda.com/anaconda/install/  

Don't forget to add path to anaconda package to your OS environment.  

*If you have enough experience with python you can skip this step. Just install python if it's not installed. 
  
### 2. Open a terminal or CMD and install required packages

Required packages:

- pyqt  
- numpy  
- matplotlib  
- scipy  


Installation:  
:~$ conda install pyqt numpy matplotlib scipy

'pip install ...' can be used instead. 

### 3. Run the software:

Go to the folder with the software and run from a terminal/CMD:  
:~$ python main_window.py

### 4. How to use (brief instruction):

1. Click the “Load” button and select a .txt file you need. Use the same file format as in the original software.
You will see the list of traces on the left panel. Click on a trace radiobutton to display another trace.
2. Click the “Find” button to perform automatic processing.
It will take some time (depend on a trace number).
3. Use the “edit jumps” panel box to add/delete jumps. Select mode on the panel (add positive, del positive, add negative, del negative, Interact). Basic mode is “Interact”, in this mode you can make different manipulations with matplotlib window (ex. zoom), in other modes you can only add/delete, so don’t forget to switch mode state before you will return to standard manipulations with matplitlib.
4. Use the “Style settings” panel box to change color/font size of the matplotlib window.
Colors: r – reed, b – blue, g – green, a – aqua, y – yellow, p – purple.
5. Click the “Save” button to save histograms. Use the “RFolder” field to create folder in which the results will be saved.
6. Click the “Backup” button to dump pickle files with the software state (can be used with the original software).


