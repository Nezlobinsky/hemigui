from PyQt5 import QtWidgets, QtCore


class LogWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        self.log_label       = QtWidgets.QLabel("Log", self)
        self.plain_text_edit = QtWidgets.QPlainTextEdit(self)

        self._main_layer = QtWidgets.QVBoxLayout()
        self._main_layer.addWidget(self.log_label)
        self._main_layer.addWidget(self.plain_text_edit)

        self.setLayout(self._main_layer)

        self.setFixedWidth(220)

    def add_entry(self, text):
        self.plain_text_edit.appendPlainText(text + "\n")
