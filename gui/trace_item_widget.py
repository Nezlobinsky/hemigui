from PyQt5 import QtWidgets, QtCore


class ColoredBoxWidget(QtWidgets.QPushButton):
    def __init__(self, color_string, parent=None):
        QtWidgets.QLineEdit.__init__(self, parent)
        self._lines_style_sheet = "background: rgb%s;" \
                                  "border-radius: 1px;" \
                                  "max-height: 1.0em;" % str(tuple(color_string))

        self.setStyleSheet(self._lines_style_sheet)



class TraceItemWidget(QtWidgets.QWidget):

    _on_include_chbox_signal = QtCore.pyqtSignal(name="onIncludeChbox")

    def __init__(self, trace_num, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        self._trace_num = trace_num

        self._main_layout = QtWidgets.QHBoxLayout()
        self._trace_label = QtWidgets.QLabel("trace " + str(trace_num), self)
        self._trace_label.setFixedWidth(60)
        self.display_chbox = QtWidgets.QRadioButton(self)
        self.include_chbox = QtWidgets.QCheckBox(self)
        self._main_layout.addWidget(self._trace_label)
        self._main_layout.addWidget(self.display_chbox)
        self._main_layout.addWidget(self.include_chbox)
        self._main_layout.setContentsMargins(2, 5, 0, 5)

        self.include_chbox.stateChanged.connect(self.on_include_chbox)

        self.setLayout(self._main_layout)
  
        self.include_chbox.setChecked(True)

        # self.setFixedWidth(150)

    # def get_display_state(self):
    #     return self.display_chbox.isChecked()
    #
    # def set_display_state(self, state):
    #     self.display_chbox.setChecked(state)
    #
    def on_include_chbox(self):
        self._on_include_chbox_signal.emit()

