from PyQt5 import QtWidgets, QtCore
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import numpy as np


class MplWidget(FigureCanvas):

    _mpl_on_mouse_click_signal = QtCore.pyqtSignal(name="mplOnMouseClick")

    def __init__(self, parent=None, width=13, height=13, dpi=100):
        self._fig = Figure(figsize=(width, height), dpi=dpi)
        self._axes = self._fig.add_subplot(111)

        FigureCanvas.__init__(self, self._fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        # World 2d coordinates:
        self._x = 0
        self._y = 0

        self.font_size = 18

        self._toolbar = NavigationToolbar(self, self, coordinates=True)

        self.mpl_connect("button_press_event", self._catch_mouse_click)

    def plot_data(self, time, data, color, alpha=1.0, marker="-", markersize=1):
        if alpha > 1.0: alpha = 1.0
        if alpha < 0.0: alpha = 0.0
        if marker == "-":
            self._axes.plot(time, data, color=color, alpha=alpha)
        else:
            self._axes.plot(time, data, color=color, alpha=alpha, marker=marker, markersize=markersize, linestyle="None")

        self._fig.suptitle("", fontsize=self.font_size)
        self._axes.set_xlabel("time (s)", fontsize=self.font_size)
        self._axes.set_ylabel("Conductance (pS)", fontsize=self.font_size)
        self.draw()

    def plot_jumps(self, jumps, jumps_val, time, data, type, color):
        # jumps:
        if not jumps:
            return

        for i, ind in enumerate(jumps):
            ind = int(ind)
            self._axes.plot(time[ind], data[ind], linestyle="None", marker=".", color=color, markersize=12)
            if type == "p":
                self._axes.plot(time[ind], data[ind], linestyle="None", marker=".", color="red", markersize=6)
                self._axes.text(time[ind], data[ind], int(jumps_val[i]), color="red")
            else:
                self._axes.plot(time[ind], data[ind], linestyle="None", marker=".", color="green", markersize=6)
                self._axes.text(time[ind], data[ind], -int(jumps_val[i]), color="green")

        self.draw()

    def clear_plot(self):
        self._axes.cla()
        self.draw()

    def get_mouse_coordinates(self):
        return [self._x, self._y]

    def _catch_mouse_click(self, event):
        self._x = event.xdata
        self._y = event.ydata
        self._mpl_on_mouse_click_signal.emit()

    def get_xlim(self):
        return self._axes.get_xlim()

    def get_ylim(self):
        return self._axes.get_ylim()

    def set_xlim(self, xlim):
        print (xlim)
        self._axes.set_xlim(xlim)
 
    def set_ylim(self, ylim):
        self._axes.set_ylim(ylim)

