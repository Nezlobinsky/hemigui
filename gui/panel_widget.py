from PyQt5 import QtWidgets, QtCore


class PanelWidget(QtWidgets.QWidget):

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        self.load_data_button   = QtWidgets.QPushButton("Load", self)
        self.load_data_label    = QtWidgets.QLabel("", self)
        self.state_button       = QtWidgets.QPushButton("State", self)
        self.data_base_label    = QtWidgets.QLabel("Data base", self)
        self.data_base_edit     = QtWidgets.QLineEdit(self)
        self.save_backup_button = QtWidgets.QPushButton("Backup", self)
        self.save_result_button = QtWidgets.QPushButton("Results", self)
        self.save_results_label = QtWidgets.QLabel("RFolder:", self)
        self.save_results_edit  = QtWidgets.QLineEdit(self)

        # tooltips
        self.load_data_button.setToolTip("Load raw data traces for processing.")
        self.state_button.setToolTip("Load a previous state (backuped data).")
        self.save_backup_button.setToolTip("Save intermediate processing results: positive/negative jumps and database.")
        self.save_result_button.setToolTip("Save histograms: negative jumps, positive jumps and united jumps.")

        self._main_layer = QtWidgets.QHBoxLayout()
        self._main_layer.addWidget(self.load_data_button)
        self._main_layer.addWidget(self.load_data_label)
        self._main_layer.addWidget(self.state_button)
        self._main_layer.addWidget(self.data_base_label)
        self._main_layer.addWidget(self.data_base_edit)
        self._main_layer.addWidget(self.save_results_label)
        self._main_layer.addWidget(self.save_results_edit)
        self._main_layer.addWidget(self.save_result_button)
        self._main_layer.addWidget(self.save_backup_button)

        self.setLayout(self._main_layer)
