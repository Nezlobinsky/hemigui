from PyQt5 import QtWidgets, QtCore
from gui.trace_item_widget import TraceItemWidget


class TracesListWidget(QtWidgets.QWidget):

    _on_tvd_chbox_signal  = QtCore.pyqtSignal(name="onTvdChboxes")
    _on_base_chbox_signal = QtCore.pyqtSignal(name="onBaseChboxes")
    _on_thr_chbox_signal  = QtCore.pyqtSignal(name="onThrChboxes")
    _on_interv_chbox_signal  = QtCore.pyqtSignal(name="onIntervChboxes")
    
    _on_include_chbox_signal  = QtCore.pyqtSignal(name="onIncludeChboxes")

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        self._traces_list = QtWidgets.QListWidget(self)
        self.tvd_label    = QtWidgets.QLabel("Display tvd:", self)
        self.tvd_chbox    = QtWidgets.QCheckBox(self)
        self.base_label   = QtWidgets.QLabel("Display fit:", self)
        self.base_chbox   = QtWidgets.QCheckBox(self)
        self.thr_label    = QtWidgets.QLabel("Display thr:", self)
        self.thr_chbox    = QtWidgets.QCheckBox(self)
        self.interv_label    = QtWidgets.QLabel("Display intervals:", self)
        self.interv_chbox    = QtWidgets.QCheckBox(self)
        self.button_group = QtWidgets.QButtonGroup(self)

        # self._display_all_button = QtWidgets.QPushButton("Show all", self)
        # self._hide_all_button = QtWidgets.QPushButton("Hide all", self)

        self._main_layout = QtWidgets.QGridLayout()
        self._main_layout.addWidget(self._traces_list, 0, 0, 3, 2)
        self._main_layout.addWidget(self.tvd_label,    3, 0, 1, 1)
        self._main_layout.addWidget(self.tvd_chbox,    3, 1, 1, 1)
        self._main_layout.addWidget(self.base_label,   4, 0, 1, 1)
        self._main_layout.addWidget(self.base_chbox,   4, 1, 1, 1)
        self._main_layout.addWidget(self.thr_label,    5, 0, 1, 1)
        self._main_layout.addWidget(self.thr_chbox,    5, 1, 1, 1)
        self._main_layout.addWidget(self.interv_label, 6, 0, 1, 1)
        self._main_layout.addWidget(self.interv_chbox, 6, 1, 1, 1)

        self.traces_count = 0
        self.colors_list = []

        self.setLayout(self._main_layout)
        # self._main_layout.setContentsMargins(0, 0, 0, 0)

        self.setFixedWidth(220)

        self._traces_list.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)

        self.tvd_chbox.stateChanged.connect(self._on_tvd_chbox)
        self.base_chbox.stateChanged.connect(self._on_base_chbox)
        self.thr_chbox.stateChanged.connect(self._on_thr_chbox)
        self.interv_chbox.stateChanged.connect(self._on_interv_chbox)

    def construct_traces_list(self):
        self._traces_list.clear()
        self._item_list = []
        self._widget_item_list = []
        for i in range(self.traces_count):
            self._item_list.append(QtWidgets.QListWidgetItem())
            self._widget_item_list.append(TraceItemWidget(i, self))
            self._widget_item_list[-1].onIncludeChbox.connect(self._on_include_chbox)
            self._traces_list.addItem(self._item_list[-1])
            self._traces_list.setItemWidget(self._item_list[-1],
                               self._widget_item_list[-1])
            self._item_list[-1].setSizeHint(self._widget_item_list[-1].sizeHint())
        self._construct_trace_group()

    def _on_tvd_chbox(self):
        self._on_tvd_chbox_signal.emit()

    def _on_base_chbox(self):
        self._on_base_chbox_signal.emit()

    def _on_thr_chbox(self):
        self._on_thr_chbox_signal.emit()

    def _on_interv_chbox(self):
        self._on_interv_chbox_signal.emit()

    def _construct_trace_group(self):
        for i in range(self.traces_count):
            self.button_group.addButton(self._widget_item_list[i].display_chbox, i)

    def check_trace_state(self, i):
        return self._widget_item_list[i].include_chbox.isChecked()

    def set_trace_state(self, i, state):
        self._widget_item_list[i].include_chbox.setChecked(state) 

    def _on_include_chbox(self):
        self._on_include_chbox_signal.emit()
