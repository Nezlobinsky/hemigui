from PyQt5 import QtWidgets, QtCore
# from .jumps_algorithm_widget import JumpsAlgorithmWidget


class ControlPanelWidget(QtWidgets.QWidget):

    _call_jumps_algorithm_signal = QtCore.pyqtSignal(int, dict, name="callJumpsAlgorithm")

    _call_find_jumps_signal = QtCore.pyqtSignal(name="callFindJumps")

    _save_jumps_signal = QtCore.pyqtSignal(name="saveJumps")

    #_call_update_thr_signal = QtCore.pyqtSignal(str, name="updateThr")

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        self._find_jumps_box         = QtWidgets.QGroupBox("Find jumps", self)
        self._find_jumps_layout      = QtWidgets.QGridLayout()
        self._find_jumps_t_med_label = QtWidgets.QLabel("T mf:", self)
        self._find_jumps_thr_label   = QtWidgets.QLabel("Threshold:", self)
        self._find_jumps_sec_label   = QtWidgets.QLabel("Sec:", self)
        self._find_jumps_tmax_label  = QtWidgets.QLabel("T max:", self)
        self.find_jumps_t_med_edit  = QtWidgets.QLineEdit(self)
        self.find_jumps_thr_edit    = QtWidgets.QLineEdit(self)
        self.find_jumps_sec_edit    = QtWidgets.QLineEdit(self)
        self.find_jumps_tmax_edit   = QtWidgets.QLineEdit(self)
        self.find_jumps_button      = QtWidgets.QPushButton("Find", self)
        self.find_jumps_button.setToolTip("Find jumps (positive and negative) on all data traces.")
        self._find_jumps_layout.addWidget(self._find_jumps_t_med_label, 0, 0, 1, 1)
        self._find_jumps_layout.addWidget(self.find_jumps_t_med_edit,   0, 1, 1, 1)
        self._find_jumps_layout.addWidget(self._find_jumps_thr_label,   1, 0, 1, 1)
        self._find_jumps_layout.addWidget(self.find_jumps_thr_edit,     1, 1, 1, 1)
        self._find_jumps_layout.addWidget(self._find_jumps_sec_label,   2, 0, 1, 1)
        self._find_jumps_layout.addWidget(self.find_jumps_sec_edit,     2, 1, 1, 1)
        self._find_jumps_layout.addWidget(self._find_jumps_tmax_label,   3, 0, 1, 1)
        self._find_jumps_layout.addWidget(self.find_jumps_tmax_edit,     3, 1, 1, 1)
        self._find_jumps_layout.addWidget(self.find_jumps_button,       4, 0, 1, 1)
        self._find_jumps_box.setLayout(self._find_jumps_layout)

        self._interaction_box                 = QtWidgets.QGroupBox("Edit jumps", self)
        self._interaction_layout              = QtWidgets.QGridLayout()
        self._interaction_add_positive_button = QtWidgets.QPushButton("Add positive", self)
        self._interaction_add_positive_button.setCheckable(True)
        self._interaction_del_positive_button = QtWidgets.QPushButton("Del positive", self)
        self._interaction_del_positive_button.setCheckable(True)
        self._interaction_add_negative_button = QtWidgets.QPushButton("Add negative", self)
        self._interaction_add_negative_button.setCheckable(True)
        self._interaction_del_negative_button = QtWidgets.QPushButton("Del negative", self)
        self._interaction_del_negative_button.setCheckable(True)
        self._interaction_size_edit = QtWidgets.QLineEdit()
        self._interaction_size_edit.setAlignment(QtCore.Qt.AlignCenter)
        self._interaction_interact_button     = QtWidgets.QPushButton("Interact", self)
        self._interaction_interact_button.setCheckable(True)
        self._interaction_layout.addWidget(self._interaction_add_positive_button, 0, 0, 1, 1)
        self._interaction_layout.addWidget(self._interaction_del_positive_button, 0, 1, 1, 1)
        self._interaction_layout.addWidget(self._interaction_add_negative_button, 1, 0, 1, 1)
        self._interaction_layout.addWidget(self._interaction_del_negative_button, 1, 1, 1, 1)
        self._interaction_layout.addWidget(self._interaction_interact_button,     2, 0, 1, 1)
        self._interaction_layout.addWidget(self._interaction_size_edit, 2, 1, 1, 1)
        self._interaction_box.setLayout(self._interaction_layout)

        self._interaction_button_group = QtWidgets.QButtonGroup(self)
        self._interaction_button_group.addButton(self._interaction_add_positive_button, 0)
        self._interaction_button_group.addButton(self._interaction_del_positive_button, 1)
        self._interaction_button_group.addButton(self._interaction_add_negative_button, 2)
        self._interaction_button_group.addButton(self._interaction_del_negative_button, 3)
        self._interaction_button_group.addButton(self._interaction_interact_button,     4)

        self._advanced_settings_box     = QtWidgets.QGroupBox("Advanced settings", self)
        self._advanced_settings_layout  = QtWidgets.QGridLayout()
        self._advanced_fit_label        = QtWidgets.QLabel("Select fit:", self)
        self._advanced_fit_box          = QtWidgets.QComboBox(self)
        self._advanced_fit_remove_label = QtWidgets.QLabel("From - To (sec):", self)
        self._advanced_fit_remove_edit  = QtWidgets.QLineEdit(self)
        self._advanced_fit_remove_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.advanced_fit_button        = QtWidgets.QPushButton("Compute one", self)
        self.advanced_fit_all_button    = QtWidgets.QPushButton("Compute all", self)        

        self._advanced_settings_layout.addWidget(self._advanced_fit_label, 0, 0, 1, 1)
        self._advanced_settings_layout.addWidget(self._advanced_fit_box,   0, 1, 1, 1)
        self._advanced_settings_layout.addWidget(self._advanced_fit_remove_label, 1, 0, 1, 1)
        self._advanced_settings_layout.addWidget(self._advanced_fit_remove_edit, 1, 1, 1, 1)
        self._advanced_settings_layout.addWidget(self.advanced_fit_button, 2, 1, 1, 1)
        self._advanced_settings_layout.addWidget(self.advanced_fit_all_button, 3, 1, 1, 1)

        self._advanced_settings_box.setLayout(self._advanced_settings_layout)

        self._open_prob_box    = QtWidgets.QGroupBox("Open probability", self)
        self._open_prob_layout = QtWidgets.QGridLayout()
        self._open_prob_label  = QtWidgets.QLabel("Threshold:", self)
        self.open_prob_edit   = QtWidgets.QLineEdit(self)
        self.open_prob_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.open_prob_val_label        = QtWidgets.QLabel("Open prob:", self)
        self.open_prob_val_actual_label = QtWidgets.QLabel("", self)      
        self.open_prob_val_actual_label.setAlignment(QtCore.Qt.AlignCenter)
        self.open_prob_update_button = QtWidgets.QPushButton("Update", self)
  
        self._open_prob_layout.addWidget(self._open_prob_label, 0, 0, 1, 1)
        self._open_prob_layout.addWidget(self.open_prob_edit, 0, 1, 1, 2)
        self._open_prob_layout.addWidget(self.open_prob_val_label, 1, 0, 1, 1)
        self._open_prob_layout.addWidget(self.open_prob_val_actual_label, 1, 1, 1, 1)
        self._open_prob_layout.addWidget(self.open_prob_update_button, 1, 2, 1, 1)

        self._open_prob_box.setLayout(self._open_prob_layout)
        
        self._style_box          = QtWidgets.QGroupBox("Style settigs", self)
        self._style_layout       = QtWidgets.QGridLayout()
        self._style_colors_label = QtWidgets.QLabel("Colors: r, g, b, a, y, p", self)
        self._style_data_label   = QtWidgets.QLabel("Data color:", self)
        self.style_data_edit     = QtWidgets.QLineEdit(self)
        self.transp_data_edit    = QtWidgets.QLineEdit(self)
        self._style_sub_label    = QtWidgets.QLabel("Sub color:", self)
        self.style_sub_edit      = QtWidgets.QLineEdit(self)
        self.transp_sub_edit     = QtWidgets.QLineEdit(self)
        self._style_fit_label    = QtWidgets.QLabel("Fit color:", self)
        self.style_fit_edit      = QtWidgets.QLineEdit(self)
        self.transp_fit_edit     = QtWidgets.QLineEdit(self)
        self._style_base_label   = QtWidgets.QLabel("Base color:", self)
        self.style_base_edit     = QtWidgets.QLineEdit(self)
        self.transp_base_edit    = QtWidgets.QLineEdit(self)
        self._style_thr_label    = QtWidgets.QLabel("Threshold color:", self)
        self.style_thr_edit      = QtWidgets.QLineEdit(self)
        self.transp_thr_edit     = QtWidgets.QLineEdit(self)
        self._style_font_label   = QtWidgets.QLabel("Font size:", self)
        self.style_font_edit     = QtWidgets.QLineEdit(self)
        self.style_update_button = QtWidgets.QPushButton("Update", self)
        self._style_box.setLayout(self._style_layout)

        self._style_layout.addWidget(self._style_colors_label, 0, 0, 1, 2)
        self._style_layout.addWidget(self._style_data_label,   1, 0, 1, 1)
        self._style_layout.addWidget(self.style_data_edit,     1, 1, 1, 1)
        self._style_layout.addWidget(self.transp_data_edit,    1, 2, 1, 1)
        self._style_layout.addWidget(self._style_sub_label,    2, 0, 1, 1)
        self._style_layout.addWidget(self.style_sub_edit,      2, 1, 1, 1)
        self._style_layout.addWidget(self.transp_sub_edit,     2, 2, 1, 1)
        self._style_layout.addWidget(self._style_fit_label,    3, 0, 1, 1)
        self._style_layout.addWidget(self.style_fit_edit,      3, 1, 1, 1)
        self._style_layout.addWidget(self.transp_fit_edit,     3, 2, 1, 1)
        self._style_layout.addWidget(self._style_base_label,   4, 0, 1, 1)
        self._style_layout.addWidget(self.style_base_edit,     4, 1, 1, 1)
        self._style_layout.addWidget(self.transp_base_edit,    4, 2, 1, 1)
        self._style_layout.addWidget(self._style_thr_label,    5, 0, 1, 1)
        self._style_layout.addWidget(self.style_thr_edit,      5, 1, 1, 1)
        self._style_layout.addWidget(self.transp_thr_edit,     5, 2, 1, 1)
        self._style_layout.addWidget(self._style_font_label,   6, 0, 1, 1)
        self._style_layout.addWidget(self.style_font_edit,     6, 1, 1, 1)
        self._style_layout.addWidget(self.style_update_button, 7, 0, 1, 1)

        self._main_layer = QtWidgets.QVBoxLayout()
        self._main_layer.addWidget(self._find_jumps_box)
        self._main_layer.addWidget(self._interaction_box)
        self._main_layer.addWidget(self._advanced_settings_box)
        self._main_layer.addWidget(self._open_prob_box)
        self._main_layer.addWidget(self._style_box)

        self.traces_count = 0

        self.setLayout(self._main_layer)

        # self._main_layer.setContentsMargins(0, 0, 0, 0)
        #
        # self._list_box.currentIndexChanged.connect(self._widgets_stack.setCurrentIndex)
        # self._find_button.clicked.connect(self.call_jumps_algorithm)
        #
        # self._save_button.clicked.connect(self.save_jumps)

        # self.find_jumps_t_med_edit.setText("0.3")
        self.find_jumps_thr_edit.setText("60")
        self.find_jumps_sec_edit.setText("3")
        self.find_jumps_tmax_edit.setText("0.3")
        self.style_data_edit.setText("b")
        self.style_sub_edit.setText("o")
        self.style_fit_edit.setText("p")
        self.style_base_edit.setText("y")
        self.style_thr_edit.setText("r")
        self.style_font_edit.setText("14")

        self.transp_data_edit.setText("1.0")
        self.transp_sub_edit.setText("1.0")
        self.transp_fit_edit.setText("1.0")
        self.transp_base_edit.setText("1.0")
        self.transp_thr_edit.setText("1.0")

        self._interaction_size_edit.setText("500")
        self._advanced_fit_remove_edit.setText("0 - 0")

        self.open_prob_edit.setText("")

        self.find_jumps_t_med_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.find_jumps_thr_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.find_jumps_sec_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.find_jumps_tmax_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.style_data_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.style_sub_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.style_fit_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.style_base_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.style_thr_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.style_font_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.transp_data_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.transp_sub_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.transp_fit_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.transp_base_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.transp_thr_edit.setAlignment(QtCore.Qt.AlignCenter)

        self._interaction_interact_button.setChecked(True)

        self.construct_fit_list()

        self.tmed_values = []
        self.ind_value = 0

        self.find_jumps_t_med_edit.textEdited.connect(self.cache_tmed_value)
        #self.open_prob_edit.textEdited.connect(self._call_update_thr_signal)

    def set_jumps_algorithm_dict(self, jumps_algorithm_list):
        self._jumps_algorithm_list = jumps_algorithm_list

    def set_number_of_traces(self, n):
        self.traces_count = n

    # def construct_jumps_algorithm_list(self):
    #     self._interaction_select_trace_box.clear()
    #     # for elem in self._jumps_algorithm_list:
    #     #     # elem structure: { func_obj(reference), name(str), param_dict(dict) }
    #     #     self._jumps_algorithm_widgets_list.append(JumpsAlgorithmWidget(elem["parameters"]))
    #     #     self._list_box.addItem(elem["name"])
    #     #     self._widgets_stack.addWidget(self._jumps_algorithm_widgets_list[-1])

    def construct_fit_list(self):
        self._advanced_fit_box.addItem("fitexp")
        self._advanced_fit_box.addItem("fitexp2")
        self._advanced_fit_box.addItem("fitln")
        self._advanced_fit_box.addItem("fitlin")

    def call_jumps_algorithm(self):
        current_index = self._widgets_stack.currentIndex()
        param_dict = self._jumps_algorithm_widgets_list[current_index].get_parameters()
        self._call_jumps_algorithm_signal.emit(current_index, param_dict)

    def get_current_state_and_trace(self):
        id_ = self._interaction_button_group.checkedId()
        if id_ == 0:
            state = "add_p"
        elif id_ == 1:
            state = "del_p"
        elif id_ == 2:
            state = "add_n"
        elif id_ == 3:
            state = "del_n"
        elif id_ == 4:
            state = "interact"
        else:
            state = "interact"

        return [state, 0]

    def get_current_fit_state(self):
        return self._advanced_fit_box.currentText()

    def construct_tmed_list(self, traces_count):
        self.tmed_values = ["0.3"]*traces_count

    def restore_tmed_value(self, i):
        if self.tmed_values:
            self.find_jumps_t_med_edit.setText(self.tmed_values[i])
            self.ind_value = i

    def cache_tmed_value(self, v):
        self.tmed_values[self.ind_value] = v

    def get_size_window(self):
        val = self._interaction_size_edit.text()
        try:
            val = int(val)
        except Exception:
            pass
        return val

    def get_remove_first_sec(self):
        return self._advanced_fit_remove_edit.text()
        #try:
        #    val = float(val)
        #except Exception:
        #    val = 0
        #return val

    def get_open_prob_threshold(self):
        val = self.open_prob_edit.text()
        try:
            val = float(val)
        except Exception:
            val = 0
        return val

    def set_open_prob(self, thr, val):
        self.open_prob_edit.setText(thr)
        self.open_prob_val_actual_label.setText(val)


if __name__ == '__main__':

    import sys

    app = QtWidgets.QApplication(sys.argv)
    mainWin = FindJumpsWidget()
    mainWin.show()
    sys.exit(app.exec_())
